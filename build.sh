#!/bin/sh

echo "===> Building the front end app"
cd wolt-clone-front-end/wolt-clone
npm install --force
ng build

echo "===> Building the services"
docker-compose up --force-recreate --build
